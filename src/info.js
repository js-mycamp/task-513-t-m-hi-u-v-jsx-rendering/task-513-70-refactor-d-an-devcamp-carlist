import { gJsonCars } from "./info";



// Chuyển đổi chuỗi JSON thành mảng đối tượng
const Cars = JSON.parse(gJsonCars);
 
 function App() {
  return (
    <div>
      <ul>
        {Cars.map((car,index)=>{
         return <li key={index} >
          Model: {car.model }      Id: {car.vID}   Car type : {car.year >= 2018? "New":"Old"}
         </li>
        })}
      </ul>
    </div>
  );
}


export default App;
